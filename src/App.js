import './App.css';
import {useState} from 'react';
import Header from './Components/Header/Header.jsx';
import Body from './Components/Body/Body.jsx';
import Footer from './Components/Footer/Footer.jsx';

const png = 'image/png';
const jpeg = 'image/jpeg';
const gif = 'image/gif';

function App() {
    const [drag, setDrag] = useState(false);
    const [fileInfo, setFileInfo] = useState([]);
    const [selected, setSelected] = useState(false);

    const dragStartHandler = (e) => {
        e.preventDefault();
        setDrag(true);
    }

    const dragLeaveHandler = (e) => {
        e.preventDefault();
        setDrag(false);
    }

    const dropHandler = (e) => {
        e.preventDefault();

        for (let i = 0; i < e.dataTransfer.files.length; i++) {
            if (e.dataTransfer.files[i].type === png ||
                e.dataTransfer.files[i].type === jpeg ||
                e.dataTransfer.files[i].type === gif) {
                setFileInfo(prevInfo => [...prevInfo, {
                    name: e.dataTransfer.files[i].name,
                    size: e.dataTransfer.files[i].size,
                    url: window.URL.createObjectURL(e.dataTransfer.files[i]),
                }]);
            } else {
                setDrag(false);
                return;
            }
        }
        setDrag(false);
        setSelected(true);
    }

    const addFileHandler = () => {
        let fileInfo = document.querySelector('#file').files;

        for (let i = 0; i < fileInfo.length; i++) {
            if (fileInfo[i].type === png ||
                fileInfo[i].type === jpeg ||
                fileInfo[i].type === gif) {
                setFileInfo(prevInfo => [...prevInfo, {
                    name: fileInfo[i].name,
                    size: fileInfo[i].size,
                    url: window.URL.createObjectURL(fileInfo[i]),
                }]);
                setSelected(true);
            } else {
                return;
            }
        }
    }

    return (
        <div className='app'
             onDragEnter={e => dragStartHandler(e)}
             onDragLeave={e => dragLeaveHandler(e)}
             onDragOver={e => dragStartHandler(e)}
             onDrop={e => dropHandler(e)}>
            <Header/>
            <Body selected={selected}
                  drag={drag}
                  addFileHandler={addFileHandler}
                  fileInfo={fileInfo}/>
            <Footer/>
        </div>
    );
}

export default App;
