import React from 'react';
import s from './Header.module.css';
import profileImg from '../../assets/images/low_no_avatar.jpg';

const Header = () => {
    return (
        <div className={s.header}>
            <div className={s.logo}>Logo</div>
            <div className={s.profile}>
                <img className={s.image} src={profileImg} alt='profile-photo' />
                <div className={s.name}>gavrilovavv</div>
                <a href='#' className={s.btn}>Выход</a>
            </div>
        </div>
    );
};

export default Header;
