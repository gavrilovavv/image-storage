import React from 'react';
import s from './Footer.module.css';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faLanguage} from '@fortawesome/free-solid-svg-icons';

const Footer = () => {
    return (
        <div className={s.footerWrapper}>
            <div className={s.footer}>
                <div className={s.selectLanguage}>
                    <FontAwesomeIcon icon={faLanguage} />
                    <select className={s.language}>
                        <option>Русский</option>
                        <option>English</option>
                    </select>
                </div>
                <ul className={s.menu}>
                    <li><a href='#' className={s.link}>
                        Условия использования
                    </a></li>
                    <li><a href='#' className={s.link}>
                        Лицензионное соглашение
                    </a></li>
                    <li><a href='#' className={s.link}>
                        Политика конфиденциальности
                    </a></li>
                </ul>
                <div>© 2023 <a href='#' className={s.copyrightLink}>Gaijin Network Ltd</a></div>
            </div>
        </div>
    );
};

export default Footer;