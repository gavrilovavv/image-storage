import React, {useState} from 'react';
import s from './CardItem.module.css';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faImage, faLink, faCode, faMarker} from '@fortawesome/free-solid-svg-icons';
import img from '../../assets/icons/magnifying-glass-plus-solid.svg';

const CardItem = (props) => {
    const [size, setSize] = useState([]);

    const getSize = (e) => {
        setSize([{
            width: e.target.naturalWidth,
            height: e.target.naturalHeight,
        }]);
    }

    return (
        <div className={s.wrapper}>
            <div className={s.fileHeader}>
                <div>
                    <FontAwesomeIcon className={s.icon} icon={faImage}/>
                    {props.name}
                </div>
                <div>{props.size} Bytes</div>
                {size.map((el, index) => {
                    return <div key={index}>{el.width}x{el.height}</div>
                })}
            </div>
            <div className={s.content}>
                <img onLoad={(e) => getSize(e)} className={s.img} src={props.url} alt='img'/>
                <a className={s.linkImage} href={props.url} target='_blank' rel='noreferrer'>
                    <img className={s.iconGlass} src={img} alt='icon'/>
                </a>
                <div>
                    <div className={s.linkItem}>
                        <FontAwesomeIcon className={s.icon} icon={faLink}/>
                        <div>{props.url}</div>
                    </div>
                    <div className={s.linkItem}>
                        <FontAwesomeIcon className={s.icon} icon={faCode}/>
                        <div>{props.url}</div>
                    </div>
                    <div className={s.linkItem}>
                        <FontAwesomeIcon className={s.icon} icon={faMarker}/>
                        <div>{props.url}</div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default CardItem;