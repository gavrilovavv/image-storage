import React from 'react';
import s from './Body.module.css';
import CardItem from '../CardItem/CardItem.jsx';

const Body = (props) => {
    return (
        <div className={s.bodyWrapper} id='body'>
            {
                props.selected
                    ? <label className={s.bodySmall}>
                        <input className={s.inputFile} type='file' id='file' onChange={props.addFileHandler} multiple/>
                        {
                            props.drag
                                ? <div className={s.textDragging}>
                                    Перетащите файлы в окно браузера <br/>
                                    чтобы начать загрузку
                                    <div className={s.dragging}>
                                    </div>
                                </div>
                                : <div className={s.btn}>Загрузить картинки</div>
                        }
                    </label>
                    : <label className={s.bodyLarge}>
                        <input className={s.inputFile} type='file' id='file' onChange={props.addFileHandler} multiple/>
                        {
                            props.drag
                                ? <div className={s.textDragging}>
                                    Перетащите файлы в окно браузера <br/>
                                    чтобы начать загрузку
                                    <div className={s.dragging}>
                                    </div>
                                </div>
                                : <>
                                    <div className={s.text}>
                                        Перетащите изображения в любое место на экране<br/>
                                        или используйте кнопку
                                    </div>
                                    <div className={s.btn}>Загрузить картинки</div>
                                </>
                        }
                    </label>
            }
            <div className={s.info}>
                Можно загружать: png, gif, jpeg; размеры до 8192x8192px; максимальный размер файла — 20MB
            </div>
            {props.fileInfo.map((el, index) => {
                return <CardItem key={index} url={el.url} name={el.name} size={el.size}/>
            })}
        </div>
    );
};

export default Body;
